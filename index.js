const express = require("express");
const axios = require("axios");
const dotenv = require("dotenv");
const jwt = require("jsonwebtoken");

dotenv.config();

const app = express();
const port = 3000;

const lineLoginUrl = `https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=${
  process.env.CHANNEL_ID
}&redirect_uri=${encodeURIComponent(
  process.env.CALLBACK_URL
)}&state=12345abcde&scope=openid%20profile%20email`;

app.get("/", (req, res) => {
  res.send(`<a href="${lineLoginUrl}">Login with LINE</a>`);
});

app.get("/callback", async (req, res) => {
  const code = req.query.code;
  const tokenUrl = "https://api.line.me/oauth2/v2.1/token";

  try {
    const response = await axios.post(
      tokenUrl,
      new URLSearchParams({
        grant_type: "authorization_code",
        code: code,
        redirect_uri: process.env.CALLBACK_URL,
        client_id: process.env.CHANNEL_ID,
        client_secret: process.env.CHANNEL_SECRET,
      })
    );

    const idToken = response.data.id_token;

    // Verify and decode the ID token
    const decoded = jwt.decode(idToken);

    if (decoded) {
      const email = decoded.email || "No email found";
      const name = decoded.name || "No name found";
      const picture = decoded.picture || "No picture found";
      const userId = decoded.sub || "No userID found";

      res.send(`
                <button id="copyButton">Click to copy token</button>
                <p>Login successful!</p>
                <p>Email: ${email}</p>
                <p>Name: ${name}</p>
                <p><img src="${picture}" alt="Profile Picture"></p>
                <p>UserID: ${userId}</p>
                <p>Token: ${idToken}</p>

                <script>
                    document.addEventListener('DOMContentLoaded', function () {
                        const copyButton = document.getElementById('copyButton');
                        const tokenParagraph = document.querySelector('p:last-of-type');

                        copyButton.addEventListener('click', function () {
                            const tokenText = tokenParagraph.textContent;
                            const tempInput = document.createElement('textarea');
                            tempInput.value = tokenText;
                            document.body.appendChild(tempInput);
                            tempInput.select();
                            document.execCommand('copy');
                            document.body.removeChild(tempInput);
                            alert('Token copied to clipboard');
                        });
                    });
                </script>
            `);
    } else {
      res.send("Login successful, but no additional information found.");
    }
  } catch (error) {
    console.error(error);
    res.send("Error during login");
  }
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
